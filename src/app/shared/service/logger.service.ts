import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  logPostRequest(url: string, data: any): void {
    console.log(`POST: ${url} \nBODY: ${JSON.stringify(data)}}`);
  }
}
