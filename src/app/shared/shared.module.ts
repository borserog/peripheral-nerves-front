import { ReactiveFormsModule, FormsModule, FormGroup } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainSpinnerComponent } from './components/main-spinner/main-spinner.component';


@NgModule({
  declarations: [MainSpinnerComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule
  ],
  exports: [
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    FormsModule,
    MainSpinnerComponent
  ]
})
export class SharedModule { }
