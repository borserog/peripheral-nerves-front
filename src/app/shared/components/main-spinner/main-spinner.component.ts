import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-main-spinner',
  templateUrl: './main-spinner.component.html',
  styleUrls: ['./main-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
