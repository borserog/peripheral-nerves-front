import { PatientDetailsComponent } from './main/patient/patient-details/patient-details.component';
import { NewPatientComponent } from './main/patient/new-patient/new-patient.component';
import { PatientListComponent } from './main/patient/patient-list/patient-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/main/patients', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'main',
    component: MainComponent,
    children: [
      {
        path: 'patients',
        component: PatientListComponent,
        pathMatch: 'full',
      },
      {
        path: 'new-patient',
        component: NewPatientComponent,
        pathMatch: 'full'
      },
      {
        path: 'details/:id',
        component: PatientDetailsComponent,
      }
    ]
   },
   {path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
