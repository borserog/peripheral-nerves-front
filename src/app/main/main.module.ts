import { AppRoutingModule } from './../app-routing.module';
import { MainComponent } from './main.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { NewPatientComponent } from './patient/new-patient/new-patient.component';
import { PatientDetailsComponent } from './patient/patient-details/patient-details.component';
import { PersonalDataComponent } from './patient/patient-details/personal-data/personal-data.component';
import { PatientMrcComponent } from './patient/patient-details/patient-mrc/patient-mrc.component';
import { ParestesyComponent } from './patient/patient-details/parestesy/parestesy.component';

@NgModule({
  declarations: [
    MainComponent,
    PatientListComponent,
    NewPatientComponent,
    PatientDetailsComponent,
    PersonalDataComponent,
    PatientMrcComponent,
    ParestesyComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ]
})
export class MainModule { }
