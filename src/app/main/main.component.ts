import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class MainComponent implements OnInit {
  patientForm = new FormGroup({
    name: new FormControl(),
    rghc: new FormControl(),
    occupation: new FormControl(),
    laterality: new FormControl(),
    birthdate: new FormControl(),
    lesionSide: new FormControl(),
    firstComplaint: new FormControl(),
    firstExamination: new FormControl(),
  });

  constructor() { }

  ngOnInit() {
    this.patientForm.valueChanges.subscribe(() => console.log(this.patientForm));
  }

}
