import { environment } from './../../../../environments/environment';
import { LoggerService } from '../../../shared/service/logger.service';
import { Patient, PatientRequest } from './../model/patient.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private static readonly RESOURCE_PATIENTS = environment.url + 'patient';

  constructor(
    private httpClient: HttpClient,
    private loggerService: LoggerService
  ) {  }

  getPatients(): Observable<Patient[]> {
    return this.httpClient.get<Patient[]>(PatientService.RESOURCE_PATIENTS);
  }

  registerPatient(newPatient: PatientRequest) {
    return this.httpClient.post(PatientService.RESOURCE_PATIENTS, newPatient);
  }
}
