import { PatientRequest } from './../model/patient.model';
import { PatientService } from './../service/patient.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { defaultCardFlexValue } from '../../../layout/flex-values';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.scss']
})
export class NewPatientComponent implements OnInit, AfterViewInit, OnDestroy {
  flexValue = defaultCardFlexValue;
  maxDate: Date;
  onDestroy = new Subject<void>();
  patientSummary$;

  // Todo criar o master form que escuta a mudança em todos os forms;
  //

  personalDataForm = this.fb.group({
    name: [''],
    rghc: [''],
    occupation: [''],
    handedness: [''],
    bornDate: [''],
  });

  clinicalDataForm = this.fb.group({
    affectedSide: [''],
    complainStart: [''],
    surgeryDate: [''],
  });

  patientForm;

  constructor(
    private fb: FormBuilder,
    private patientService: PatientService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.maxDate = new Date();
    console.log(this.maxDate);
  }

  ngAfterViewInit(): void {
    // this.patientForm = combineLatest([
    //   this.personalDataForm.valueChanges,
    //   this.clinicalDataForm.valueChanges,
    //   this.protocolDataForm.valueChanges
    // ]);
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  isFormInvalid(): boolean {
    return this.personalDataForm.invalid || this.clinicalDataForm.invalid;
  }

  onSendForm(): void {
    this.patientService.registerPatient(this.getPatientFromForms()).pipe(
      takeUntil(this.onDestroy)
    ).subscribe(() => {
      this.router.navigate(['main/patients']);
    });
  }

  private getPatientFromForms(): PatientRequest {
    return {
      ...this.personalDataForm.value,
      ...this.clinicalDataForm.value,
      protocols: []
    };
  }


}
