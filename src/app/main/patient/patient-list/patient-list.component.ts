import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Patient } from './../model/patient.model';
import { PatientService } from './../service/patient.service';
import { PatientListItem } from './../model/patient-list-item.model';
import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { defaultCardFlexValue } from '../../../layout/flex-values';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss'],
})
export class PatientListComponent implements OnInit {
  flexValue = defaultCardFlexValue;

  patients$: Observable<PatientListItem[]>;

  constructor(
    private patientService: PatientService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.patients$ = this.patientService.getPatients().pipe(
      map((patients: Patient[]): PatientListItem[] => {
        return patients.map(({ id, name, rghc }): PatientListItem => {
            return {
              id,
              name,
              rghc
            };
          });
        })
    );
  }


  navigateToPatient(id: number): void {
    this.router.navigate(['details', id], { relativeTo: this.route.parent });
  }
}
