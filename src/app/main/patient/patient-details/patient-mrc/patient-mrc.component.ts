import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Protocol } from '../../model/protocol.model';

@Component({
  selector: 'app-patient-mrc',
  templateUrl: './patient-mrc.component.html',
  styleUrls: ['./patient-mrc.component.scss']
})
export class PatientMrcComponent implements OnInit {
  @Input() mrc: Protocol;

  muscule = ['Oponente', 'Abdutor', 'Flexor'];

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

}
