import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMrcComponent } from './patient-mrc.component';

describe('PatientMrcComponent', () => {
  let component: PatientMrcComponent;
  let fixture: ComponentFixture<PatientMrcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMrcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMrcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
