import { PatientService } from './../service/patient.service';
import { ActivatedRoute, } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Patient } from '../model/patient.model';
import { delay, map, switchMap, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.scss']
})
export class PatientDetailsComponent implements OnInit {
  id: number;
  patient$: Observable<Patient>;

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientService
  ) { }

  ngOnInit(): void {
    this.patient$ = this.route.params.pipe(
      switchMap((patientId) => {
        return this.patientService.getPatients().pipe(
          // TODO serviço tem que fornecer endpoint para busca de Id
          map((patients: Patient[]) => {
            return patients.find((patient) =>  patient.id === Number(patientId.id));
          })
        );
      }),
    );
  }
}
