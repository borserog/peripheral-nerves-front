import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParestesyComponent } from './parestesy.component';

describe('ParestesyComponent', () => {
  let component: ParestesyComponent;
  let fixture: ComponentFixture<ParestesyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParestesyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParestesyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
