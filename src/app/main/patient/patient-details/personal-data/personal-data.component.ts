import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { Patient } from '../../model/patient.model';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
})
export class PersonalDataComponent implements OnInit {
  @Input() patient: Patient;

  constructor() {}

  ngOnInit(): void {}

  getHandednessDisplay(value: string): string {
    switch (value) {
      case 'LEFT':
        return 'Canhoto';
      case 'RIGHT':
        return 'Destro';
      case 'AMBIDEXTROUS':
        return 'Ambidestro';
    }
  }
}
