export interface PatientListItem {
  id: number;
  name: string;
  rghc: string;
}
