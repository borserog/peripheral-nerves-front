export interface Protocol {
  id: number;
  apposable: number;
  abductor: number;
  flexor: number;
  date: Date;
}
