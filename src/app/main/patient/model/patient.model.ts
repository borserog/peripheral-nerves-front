import { Protocol } from './protocol.model';

export interface Patient {
  id: number;
  name: 'string';
  rghc: 'string';
  occupation: 'string';
  handedness: 'string';
  bornDate: Date;
  affectedSide: 'LEFT' | 'RIGHT' | 'BOTH';
  complainStart: Date;
  surgeryDate: Date;
  protocols: Protocol[];
}

export interface PatientRequest {
  id: '0'; // não deveria ser necessário passar qualquer id
  name: 'string';
  rghc: 'string';
  occupation: 'string';
  handedness: 'string';
  bornDate: Date;
  affectedSide: 'LEFT' | 'RIGHT' | 'BOTH';
  complainStart: Date;
  surgeryDate: Date;
  protocols: Protocol[];
}

